package exercise3;

import exercise2.Item;

/**
 *
 * @author nbuser
 */
public interface ItemErrorHandler {
    void handleItem(Item item);
}
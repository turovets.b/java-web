package exercise3;

import exercise2.Item;

/**
 *
 * @author nbuser
 */
public interface ItemValidator {
    boolean isValid(Item item);
}
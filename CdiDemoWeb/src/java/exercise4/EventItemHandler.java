package exercise4;

import exercise2.Item;
import exercise3.ItemErrorHandler;
import javax.enterprise.event.Event;
import javax.inject.Inject;

/**
 *
 * @author nbuser
 */
@Notify
public class EventItemHandler implements ItemErrorHandler {

    @Inject
    private Event<Item> itemEvent;

    @Override
    public void handleItem(Item item) {
        System.out.println("Firing Event");
        itemEvent.fire(item);
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.omsu.hello;

/**
 *
 * @author Turovets Bogdan
 */
public class NameHandler {
    private String name, email;
    private int age;

    public NameHandler() {
        this.name = null;
        this.email = null;
        this.age = 0;
    }

    /**
     * @return the name
     */
    public String getName() {
        return new StringBuilder(name).reverse().toString();
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }
    
     
}

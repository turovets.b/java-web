<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : index
    Created on : 04.09.2017, 17:35:02
    Author     : Turovets Bogdan.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<meta charset="utf-8">
<title>Index JSP Page</title>

<style>
  .form {
    width: 40%;
    margin: 5% auto;
    border: 1px solid #444;
    display: flex;
    align-items: stretch;
  }
</style>

<form name="jsp-response-form" action="response.jsp" class="form">
  <p class="form__item">
    <label class="form__label">
      Enter your name: <input name="name" required>
    </label>
  </p>
  <p class="form__item">
    <label class="form__label">
      Enter your age: <input name="age" type="number" min="0" max="200" size="3" required>
    </label>
  </p>
  <p class="form__item">
    <label class="form__label">
      Enter your e-mail: <input name="email" type="email" required>
    </label>
  </p>
  <input name="submit" type="submit" value="OK" required class="form__item">
</form>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : response
    Created on : 04.09.2017, 17:30:02
    Author     : Turovets Bogdan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<meta charset=UTF-8">
<title>Responce JSP Page</title>

<style>
  .response {
    width: 40%;
    margin: 5% auto;
    border: 1px solid #444;
    display: flex;
    flex-direction: column;
  }
</style>

<jsp:useBean id="mybean" scope="session" class="ru.omsu.hello.NameHandler" />
<jsp:setProperty name="mybean" property="name" />
<jsp:setProperty name="mybean" property="age" />
<jsp:setProperty name="mybean" property="email" />

<main class="response">
  <div class="response__item">
    <p class="response__p">Hello, <b><i><jsp:getProperty name="mybean" property="name" /></i></b>!
  </div>
  <div class="response__item">
    <p class="response__p">Your age is <b><i><jsp:getProperty name="mybean" property="age" /></i></b>!
  </div>
  <div class="response__item">
    <p class="response__p">Your e-mail is <b><i><jsp:getProperty name="mybean" property="email" /></i></b>.
  </div>
</main>
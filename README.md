# Веб-кодирование

## На языке Java

| № | Ссылка | Инструкция |
|---|--------|------------|
| 1 | [HelloWeb](HelloWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/quickstart-webapps_ru.html) |
| 2 | [StrutsWeb](StrutsWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/quickstart-webapps-struts_ru.html) |
| 3 | [MySQLWeb](MySQLWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/mysql-webapp_ru.html) |
| 4 | [ServletListenerWeb](ServletListenerWeb/) | См. презентацию "Лабораторная работа web-приложение" |
| 5 | [JavaEEFacesWeb](JavaEEFacesWeb/) | [netbeans.org](https://netbeans.org/kb/docs/javaee/javaee-gettingstarted_ru.html) |
| 6 | [ConsultingAgencyWeb](ConsultingAgencyWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/jsf20-crud_ru.html) |
| 7 | [CdiDemoWeb](CdiDemoWeb/) | [netbeans.org](https://netbeans.org/kb/docs/javaee/cdi-intro_ru.html) |
| 8 | [HelloSpringWeb](HelloSpringWeb/) | [netbeans.org](https://netbeans.org/kb/docs/web/quickstart-webapps-spring_ru.html) |
| 9 | [SpringIdolWeb](SpringIdolWeb/) | См. презентацию "Spring в действии 2" или книгу Walls C. "Spring In Action" |
| 10 | [SpringBootHelloWorldWeb](SpringBootHelloWorldWeb/) | [mkyong.com](http://www.mkyong.com/spring-boot/spring-boot-hello-world-example-jsp/) |

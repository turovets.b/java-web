package turovets_b.xspringidolweb;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XSpringIdolWeb {

  public static void main(String[] args) {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("/spring-idol.xml");
    Performer performer = (Performer) ctx.getBean("duke");
    performer.perform();
  }
}

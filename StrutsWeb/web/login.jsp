<%-- 
    Document   : login
    Created on : Oct 22, 2013, 10:34:33 PM
    Author     : davidsmith
--%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login Form</title>
        <link rel="stylesheet" href="stylesheet.css">
    </head>
    <body>
        <h1>Login Form</h1>
        <html:form action="/login">
            <table border="0">
                <tr>
                    <td colspan="2"><bean:write name="LoginForm" property="error" filter="false"/></td>
                </tr>
                <tr>
                    <td>Enter your name:</td>
                    <td><html:text property="name" /></td>
                </tr>
                <tr>
                    <td>Enter your email:</td>
                    <td><html:text property="email" /></td>
                </tr>
                <tr>
                    <td colspan="2"><html:submit value="Login" /></td>
                </tr>
            </table>
        </html:form>
    </body>
</html>
